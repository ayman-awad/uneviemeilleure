<?php
namespace App\Utils;

class PasswordUtils {

    /**
     * Return bcrypt string for password
     */
    public static function generatePassword(string $value) {
        return password_hash($value, PASSWORD_DEFAULT);
    }

}