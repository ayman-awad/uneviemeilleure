<?php

namespace App\Utils;

use App\Service\Calendar;

class CalendarUtils
{

    public static function getCurentDate()
    {
        return  new \DateTime("now");
    }

    public static function getCurrentMonth()
    {
        return intval(CalendarUtils::getCurentDate()->format("m"));
    }

    public static function getCurrentYear()
    {
        return intval(CalendarUtils::getCurentDate()->format("Y"));
    }

    public static function getIntervalYear(int $yearEnd)
    {
        return range(self::getCurrentYear(), $yearEnd);
    }

    public static function getNameInFrench()
    {
        $nameInFrench = [];
        $calendar = Calendar::getCurrentCalendar();
        $numberDay = intval($calendar->getNumberDayInMonth());
        for ($i = 1; $i <= $numberDay; $i++) {
            array_push($nameInFrench, $calendar->getNameDayByDate(CalendarUtils::getCurrentMonth(), $i, CalendarUtils::getCurrentYear()));
        }

        return $nameInFrench;
    }

    public static function getBuildIntervalDate(int $range){
        return range(self::getCurrentYear(), self::getCurrentYear() + $range);
    }
}
