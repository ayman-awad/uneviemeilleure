<?php

namespace App\Repository;

use App\Entity\Ecours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Ecours|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ecours|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ecours[]    findAll()
 * @method Ecours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EcoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ecours::class);
    }

    // /**
    //  * @return Ecours[] Returns an array of Ecours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ecours
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
