<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\Place;
use App\Form\PlaceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlaceController extends AbstractController
{
    /**
     * @Route("/admin/Lieu", name="place")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Place::class);
        $place = $repository->findAll();

        return $this->render('administrator/place/index.html.twig', [
            'places' => $place,
        ]);
    }

    /**
     * @Route("/admin/AjouterLieu",name="add-place")
     */
    public function addLieu(Request $request)
    {

        $place = new Place();
        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $idAdmin = $this->getUser()->getId();
            $repository = $this->getDoctrine()->getRepository(Admin::class);
            $admin = $repository->find($idAdmin);

            $entityManager = $this->getDoctrine()->getManager();

            $place->setAdmin($admin);

            $entityManager->persist($place);
            $entityManager->flush();

            return $this->redirectToRoute('place');
        }

        return $this->render('administrator/place/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/SupprimerLieu/{id}", name="delete-place")
     */
    public function deleteLieu(Place $place)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($place);
        $entityManager->flush();

        return $this->redirectToRoute('place');
    }


    /**
     * @Route("/admin/ModifierLieu/{id}", name="update-place")
     */
    public function updateLieu(Request $request, Place $place)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->flush();

            return $this->redirectToRoute('place');
        }

        return $this->render('administrator/place/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
