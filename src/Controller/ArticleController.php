<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/admin/article", name="article")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $articles = $repository->findAll();

        return $this->render('administrator/article/index.html.twig',  array(
                'articles' => $articles,

            )
        );
    }

    /**
     * @Route("/admin/AjouterArticle", name="add-article")
     */
    public function addArticle(Request $request){

        $article = new Article();
        $form = $this->createForm(ArticleType::class,$article);
        $form->handleRequest($request);

       if($form->isSubmitted() && $form->isValid()){

            $idAdmin = $this->getUser()->getId();
            $admin = $this->getDoctrine()->getRepository(Admin::class)->find($idAdmin);
            $article->setAdmin($admin);
            $article->setDateCreation(new \DateTime);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

        return $this->redirectToRoute('article');
        }

        return $this->render('administrator/article/add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/admin/VoirArticle/{id}", name="see-article")
     */
    public function seeArticle(Article $article){
    
        return $this->render('administrator/article/see.html.twig',  array(
                'article' => $article,
            )
        );
    }

    /**
     * @Route("/VoirArticle/{id}", name="see-home-article")
     */
    public function seeHomeArticle(Article $article){
    
        return $this->render('home/article/article.html.twig',  array(
                'article' => $article,
            )
        );
    }

    /**
     * @Route("/admin/SupprimerArticle/{id}", name="delete-article")
     */
    public function deleteArticle(Article $article){

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();

        $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
   
        return $this->render('administrator/article/index.html.twig',  array(
                'articles' => $articles,
            )
        );
    }

    /**
    * @Route("/admin/ModifierArticle/{id}", name="update-article")
    */
    public function updateArticle(Request $request, Article $article){
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(ArticleType::class,$article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->flush();
            return $this->redirectToRoute('article');
        }
        return $this->render('administrator/article/update.html.twig', array(
            'form' => $form->createView()
        ));
    }


}
