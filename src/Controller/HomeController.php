<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/Parrain", name="godfather")
     */
    public function godFather()
    {

        return $this->render('home/godfather/godfather.html.twig');
    }

    /**
    * @Route("/Actualites", name="actuality")
     */
    public function actuality()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $allArticle = $entityManager->getRepository(Article::class)->findAll();

        return $this->render('@article/articles.html.twig',array (
            'articles' => $allArticle
        ));
    }

    /**
     * @Route("/Contact", name="contact")
     */
    public function contact()
    {
        return $this->render('home/contact/contact.html.twig');
    }

    /**
     * @Route("/Partenaire", name="partner")
     */
    public function partner()
    {
        return $this->render('home/partner/partner.html.twig');
    }

    /**
     * @Route("/Statut", name="status")
     */
    public function status()
    {
        return $this->render('home/status/status.html.twig');
    }
}
