<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\Patient;
use App\Form\PatientType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PatientController extends AbstractController
{
    /**
     * @Route("/patient", name="patient")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Patient::class);
        $patients = $repository->findAll();

        return $this->render('administrator/patient/index.html.twig', [
            'patients' => $patients,
        ]);
    }

    /**
     * @Route("/AjouterPatient", name="add-patient")
     */
    public function addPatient(Request $request)
    {
        $patient = new Patient();
        $form = $this->createForm(PatientType::class,$patient);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $idAdmin = $this->getUser()->getId();

            $repositoryAdmin = $this->getDoctrine()->getRepository(Admin::class);

            $admin = $repositoryAdmin->find($idAdmin);
            $patient->setAdmin($admin);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($patient);
            $entityManager->flush();

            return $this->redirectToRoute('patient');
        }
        return $this->render('administrator/patient/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/SupprimerPatient/{id}", name="delete-patient")
     */
    public function deletePatient(Patient $patient){

        $entityManager = $this->getDoctrine()->getManager();
    
        $entityManager->remove($patient);
        $entityManager->flush();

        return $this->redirectToRoute('patient');
    }

    /**
     * @Route("/admin/ModifierPatient/{id}", name="update-patient")
     */
    public function updatePatient(Request $request,Patient $patient){

        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(PatientType::class,$patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($patient);
            $entityManager->flush();
            return $this->redirectToRoute('patient');
        }

        return $this->render('administrator/patient/update.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}
