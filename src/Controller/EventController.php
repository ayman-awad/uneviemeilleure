<?php

namespace App\Controller;


use App\Entity\Event;
use App\Entity\Admin;
use App\Service\Calendar;
use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\EventType;
use App\Utils\CalendarUtils;

class EventController extends AbstractController
{
    /**
     * @Route("/admin/Sortie", name="event")
     */
    public function index(EventRepository $sortie)
    {
        $nameInFrench[] = null;
        $month = CalendarUtils::getCurrentMonth();
        $year = CalendarUtils::getCurrentYear();
        $calendar = new Calendar($month, $year);
        $currentDay = $calendar->getDay();
        $lastDayMonth = $calendar->getLastDayMonth();
        $numberDay = intval($calendar->getNumberDayInMonth());

        $dateStar = $calendar->buildStartEvent(
            $calendar->getMonth(),
            $calendar->getYear()
        );

        $dateEnd = $calendar->buildEndEvent(
            $calendar->getMonth(),
            $calendar->getYear(),
            $calendar->getLastDayMonth()
        );

        for ($i = 1; $i <= $numberDay; $i++) {
            array_push($nameInFrench, $calendar->getNameDayByDate($month, $i, $year));
        }

        return $this->render(
            'administrator/event/index.html.twig',
            [
                'calendar' => $calendar,
                'year' => $year,
                'month' =>  $calendar->getNameCurrentMonth(),
                'monthNumber' =>  $calendar->getMonth(),
                'numberDay' => $numberDay,
                'day' => $currentDay,
                'nameInFrench' => $nameInFrench,
                'lastDayMonth' => $lastDayMonth,
                'allEvents' => $sortie->getEventBetweenDate($dateStar, $dateEnd),
            ]
        );
    }

    /**
     * @Route("/admin/MoisPrecedent/{month}/{year}", name="previews-month")
     */
    public function previewsMonth($month, $year, EventRepository $sortie)
    {
        $nameInFrench[] = null;
        $calendar = new Calendar(intval($month), intval($year));
        $year = $calendar->previewsMonth()->getYear();
        $month = $calendar->previewsMonth()->getMonth();
        $calendarNext = new Calendar($month, $year);
        $numberDay = intval($calendarNext->getNumberDayInMonth());

        $dateStar = $calendarNext->buildStartEvent(
            $calendarNext->getMonth(),
            $calendarNext->getYear()
        );

        $dateEnd = $calendarNext->buildEndEvent(
            $calendarNext->getMonth(),
            $calendarNext->getYear(),
            $calendarNext->getLastDayMonth()
        );

        for ($i = 1; $i <= $numberDay; $i++) {
            array_push($nameInFrench, $calendar->getNameDayByDate($month, $i, $year));
        }

        return $this->render(
            'administrator/event/index.html.twig',
            [
                'year' => $year,
                'month' =>  $calendarNext->getNameCurrentMonth(),
                'monthNumber' => $calendarNext->getMonth(),
                'numberDay' => $numberDay,
                'nameInFrench' => $nameInFrench,
                'allEvents' => $sortie->getEventBetweenDate($dateStar, $dateEnd)
            ]
        );
    }

    /**
     * @Route("/admin/MoisSuivant/{month}/{year}", name="next-month")
     */
    public function nextMonth($month, $year, EventRepository $sortie)
    {
        $nameInFrench[] = null;
        $calendar = new Calendar(intval($month), intval($year));
        $year = $calendar->nextMonth()->getYear();
        $month = $calendar->nextMonth()->getMonth();
        $calendarNext = new Calendar($month, $year);
        $numberDay = intval($calendarNext->getNumberDayInMonth());

        $dateStar = $calendarNext->buildStartEvent(
            $calendarNext->getMonth(),
            $calendarNext->getYear()
        );

        $dateEnd = $calendarNext->buildEndEvent(
            $calendarNext->getMonth(),
            $calendarNext->getYear(),
            $calendarNext->getLastDayMonth()
        );

        for ($i = 1; $i <= $numberDay; $i++) {
            array_push($nameInFrench, $calendar->getNameDayByDate($month, $i, $year));
        }

        return $this->render(
            'administrator/event/index.html.twig',
            [
                'year' => $year,
                'month' =>  $calendarNext->getNameCurrentMonth(),
                'monthNumber' =>  $calendarNext->getMonth(),
                'numberDay' => $numberDay,
                'nameInFrench' => $nameInFrench,
                'allEvents' => $sortie->getEventBetweenDate($dateStar, $dateEnd)
            ]
        );
    }

    /**
     * @Route("/admin/AjouterSortie", name="add-event")
     */
    public function addEvent(Request $request)
    {
        $event = new Event();
        $form = $this->createForm(EventType::class, $event);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $idAdmin = $this->getUser()->getId();
            $repositoryAdmin = $this->getDoctrine()->getRepository(Admin::class);
            $admin = $repositoryAdmin->find($idAdmin);
            $event->setAdmin($admin);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            return $this->redirectToRoute('event');
        }

        return $this->render('administrator/event/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/VoirSortie/{id}", name="see-event")
     */
    public function seeEvent(Event $event)
    {
        return $this->render(
            'administrator/event/see.html.twig',
            [
                'event' => $event,
            ]
        );
    }

    /**
     * @Route("/admin/ModifierSortie/{id}", name="update-event")
     */
    public function updateEvent(Request $request, Event $event)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('event');
        }

        return $this->render('administrator/event/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/SupprimerSortie/{id}", name="delete-event")
     */
    public function deleteEvent(Event $event)
    {
        // add token
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('event');
    }

}
