<?php

namespace App\Service;


use App\Entity\Event;
use App\Utils\CalendarUtils;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use PhpParser\Node\Scalar\String_;

class Calendar
{
    private $month;
    private $day;
    private $year;
    private $numbersDayInMonth;
  
    private $listMonth = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    private $listDayNameFrench = [
        'Lundi' => 'Monday',
        'Mardi' => 'Tuesday',
        'Mercredi' => 'Wednesday',
        'Jeudi' => 'Thursday',
        'Vendredi' => 'Friday',
        'Samedi' => 'Saturday',
        'Dimanche' => 'Sunday'
    ];

    public function __construct(?int $month, ?int $year)
    {
        $this->day = date("j");
        if ($month == null && $year == null) {
            $this->year =  date("Y");
            $this->month = date("m");
        } else {
            $this->month = $month;
            $this->year = $year;
        }
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getMonth()
    {
        return $this->month;
    }

    public function getDay()
    {
        return $this->day;
    }

    public function getNameCurrentMonth()
    {
        return $this->listMonth[$this->month - 1];
    }

    public function getNumberDayInMonth()
    {
        return $this->numbersDayInMonth = cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);
    }

    public function getNameDayByDate($month, $day, $year)
    {
        $dateInfo = unixtojd(mktime(0, 0, 0, $month, $day, $year));
        $AllInfo =  cal_from_jd($dateInfo, CAL_GREGORIAN);
        $nameDayFrench = array_search($AllInfo['dayname'], $this->listDayNameFrench);
        return $nameDayFrench;
    }

    public function nextMonth(): Calendar
    {
        $month = $this->month + 1;
        $year  = $this->year;
        if ($month > 12) {
            $year  += 1;
            $month = 1;
        }
        return new Calendar($month, $year);
    }

    public function previewsMonth(): Calendar
    {
        $month = $this->month - 1;
        $year  = $this->year;
        if ($month < 1) {
            $month = 12;
            $year -= 1;
        }
        return new Calendar($month, $year);
    }

    public function getLastDayMonth()
    {
        $dateInfo =  new \DateTime("{$this->year}-{$this->month}-01");
        $lastDayNumber =  $dateInfo->modify('last day of this month');
        return intval($lastDayNumber->format('j'));
    }

    public function buildStartEvent($month, $year): string
    {
        $date = date_create();
        $date_set = date_date_set($date, $year, $month, 1);
        $dateStart =  date_format($date_set, 'Y-m-d 00:00:00');
        return $dateStart;
    }

    public function buildEndEvent($month, $year, $day): string
    {
        $date = date_create();
        $date_set = date_date_set($date, $year, $month, $day);
        $dateEnd =  date_format($date_set, 'Y-m-d 23:59:59');
        return $dateEnd;
    }

    public static function getCurrentCalendar(){
        $calendar = new Calendar(CalendarUtils::getCurrentMonth(),CalendarUtils::getCurrentYear());
        return $calendar;
    }

    



    
}
