<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Utils\PasswordUtils;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AdminFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $admin = new Admin(); 
        $admin->setFirstname('admin');
        $admin->setLastname('admin');
        $admin->setUsername('admin');
        $admin->setEmail('admin@admin.fr');
        $admin->setPassword(PasswordUtils::generatePassword('admin'));
        $manager->persist($admin);
        $manager->flush();
    }
}
