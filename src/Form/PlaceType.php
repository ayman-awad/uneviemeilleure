<?php

namespace App\Form;

use App\Entity\Place;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('namePlace',TextType::class, array('label' => 'Nom'))
            ->add('address',TextType::class, array('label' => 'Adresse'))
            ->add('town',TextType::class, array('label' => 'Ville'))
            ->add('postalCode',NumberType::class, array('label' => 'Code postal'))
            ->add('phone',NumberType::class, array('label' => 'Téléphone'))
            ->add('price',NumberType::class, array('label' => 'Prix'))
            ->add('Ajouter',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Place::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'csrf_token_id'   => 'place_item',
        ]);
    }
}
