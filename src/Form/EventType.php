<?php

namespace App\Form;

use App\Entity\Event;
use App\Entity\Patient;
use App\Entity\Place;
use App\Utils\CalendarUtils;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameEvent', TextType::class ,['label' => 'Nom sortie'])
            ->add('dateEventStart', DateTimeType::class, [
                'label' => 'Date et Heure début',
                'placeholder' => [
                    'day' => 'Jour', 'month' => 'Mois',  'year' => 'Année', 
                    'hour' => 'Heure', 'minute' => 'Minute',
                    ],
                    'years' => CalendarUtils::getBuildIntervalDate(5)
                ])
            ->add('dateEventEnd', DateTimeType::class,[
                'label' => 'Date et Heure fin',
                'placeholder' => [
                    'day' => 'Jour', 'month' => 'Mois',  'year' => 'Année', 
                    'hour' => 'Heure', 'minute' => 'Minute',
                    ],
                    'years' => CalendarUtils::getBuildIntervalDate(5)
                ])
            ->add('numberEscort', NumberType::class,['label' => 'Nombre accompagnant'])
            ->add('place', EntityType::class, [
                'class'        => Place::class,
                'choice_label' => 'namePlace',
                'multiple'     => false,
            ])
            ->add('patients', EntityType::class, [
                'class'        => Patient::class,
                'choice_label' => 'firstname',
                'multiple'     => true,
                'expanded' => true,
            ])
            ->add('Valider',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'csrf_token_id'   => 'event_item',
        ]);
    }
}
