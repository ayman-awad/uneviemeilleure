<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id",type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="nameEvent",type="string")
     */
    private $nameEvent;


    /**
     * @ORM\Column(name="dateStart",type="datetime")
     */
    private $dateEventStart;

    /**
     * @ORM\Column(name="dateEnd",type="datetime")
     */
    private $dateEventEnd;

    /**
     * @ORM\Column(name="numberEscort",type="integer")
     */
    private $numberEscort;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Place", inversedBy="events")
     * @ORM\JoinColumn(name="placefkid", referencedColumnName="id" )
     */
    private $place;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin", inversedBy="events")
     * @ORM\JoinColumn(name="adminfkid", referencedColumnName="id" )
     */
    private $admin;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Patient", inversedBy="event")
     */
    private $patients;


    public function __construct()
    {
        $this->patients = new ArrayCollection();
    }


    /**
     * @return Collection|Patient[]
     */
    public function getPatients(): Collection
    {
        return $this->patients;
    }

    public function addPatient(Patient $patient): self
    {
        if (!$this->patients->contains($patient)) {
            $this->patients[] = $patient;
            $patient->setAdmin($this);
        }

        return $this;
    }

    public function removePatient(Patient $patient): self
    {
        if ($this->patients->contains($patient)) {
            $this->patients->removeElement($patient);
            // set the owning side to null (unless already changed)
            if ($patient->getAdmin() === $this) {
                $patient->setAdmin(null);
            }
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNameEvent()
    {
        return $this->nameEvent;
    }

    /**
     * @param mixed $nameEvent
     */
    public function setNameEvent($nameEvent): void
    {
        $this->nameEvent = $nameEvent;
    }

    /**
     * @return mixed
     */
    public function getDateEventStart()
    {
        return $this->dateEventStart;
    }

    /**
     * @param mixed $dateEventStart
     */
    public function setDateEventStart($dateEventStart): void
    {
        $this->dateEventStart = $dateEventStart;
    }

    /**
     * @return mixed
     */
    public function getDateEventEnd()
    {
        return $this->dateEventEnd;
    }

    /**
     * @param mixed $dateEventEnd
     */
    public function setDateEventEnd($dateEventEnd): void
    {
        $this->dateEventEnd = $dateEventEnd;
    }

    /**
     * @return mixed
     */
    public function getNumberEscort()
    {
        return $this->numberEscort;
    }

    /**
     * @param mixed $numberEscort
     */
    public function setNumberEscort($numberEscort): void
    {
        $this->numberEscort = $numberEscort;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     */
    public function setPlace($place): void
    {
        $this->place = $place;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin): void
    {
        $this->admin = $admin;
    }

}
