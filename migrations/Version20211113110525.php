<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211113110525 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, adminkfid INT NOT NULL, category_id INT DEFAULT NULL, title VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, dateCreation DATETIME NOT NULL, INDEX IDX_23A0E6644B527AC (adminkfid), INDEX IDX_23A0E6612469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, path_img VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, placefkid INT DEFAULT NULL, adminfkid INT DEFAULT NULL, nameEvent VARCHAR(255) NOT NULL, dateStart DATETIME NOT NULL, dateEnd DATETIME NOT NULL, numberEscort INT NOT NULL, INDEX IDX_3BAE0AA7C7B259A6 (placefkid), INDEX IDX_3BAE0AA7BE076C22 (adminfkid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_patient (event_id INT NOT NULL, patient_id INT NOT NULL, INDEX IDX_9F8D7E4471F7E88B (event_id), INDEX IDX_9F8D7E446B899279 (patient_id), PRIMARY KEY(event_id, patient_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patient (id INT AUTO_INCREMENT NOT NULL, adminid INT NOT NULL, firstname VARCHAR(50) NOT NULL, lastname VARCHAR(50) NOT NULL, email VARCHAR(60) NOT NULL, mobile VARCHAR(10) NOT NULL, money DOUBLE PRECISION DEFAULT NULL, INDEX IDX_1ADAD7EBB8ED4D93 (adminid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE patient_event (patient_id INT NOT NULL, event_id INT NOT NULL, INDEX IDX_AE98D1686B899279 (patient_id), INDEX IDX_AE98D16871F7E88B (event_id), PRIMARY KEY(patient_id, event_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE place (id INT AUTO_INCREMENT NOT NULL, adminfkid INT NOT NULL, nameplace VARCHAR(50) NOT NULL, address VARCHAR(60) NOT NULL, phone VARCHAR(10) NOT NULL, price DOUBLE PRECISION DEFAULT NULL, postalcode INT NOT NULL, town VARCHAR(50) NOT NULL, INDEX IDX_741D53CDBE076C22 (adminfkid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6644B527AC FOREIGN KEY (adminkfid) REFERENCES admin (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E6612469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7C7B259A6 FOREIGN KEY (placefkid) REFERENCES place (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7BE076C22 FOREIGN KEY (adminfkid) REFERENCES admin (id)');
        $this->addSql('ALTER TABLE event_patient ADD CONSTRAINT FK_9F8D7E4471F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_patient ADD CONSTRAINT FK_9F8D7E446B899279 FOREIGN KEY (patient_id) REFERENCES patient (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EBB8ED4D93 FOREIGN KEY (adminid) REFERENCES admin (id)');
        $this->addSql('ALTER TABLE patient_event ADD CONSTRAINT FK_AE98D1686B899279 FOREIGN KEY (patient_id) REFERENCES patient (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE patient_event ADD CONSTRAINT FK_AE98D16871F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDBE076C22 FOREIGN KEY (adminfkid) REFERENCES admin (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6644B527AC');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7BE076C22');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EBB8ED4D93');
        $this->addSql('ALTER TABLE place DROP FOREIGN KEY FK_741D53CDBE076C22');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E6612469DE2');
        $this->addSql('ALTER TABLE event_patient DROP FOREIGN KEY FK_9F8D7E4471F7E88B');
        $this->addSql('ALTER TABLE patient_event DROP FOREIGN KEY FK_AE98D16871F7E88B');
        $this->addSql('ALTER TABLE event_patient DROP FOREIGN KEY FK_9F8D7E446B899279');
        $this->addSql('ALTER TABLE patient_event DROP FOREIGN KEY FK_AE98D1686B899279');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7C7B259A6');
        $this->addSql('DROP TABLE admin');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE event_patient');
        $this->addSql('DROP TABLE patient');
        $this->addSql('DROP TABLE patient_event');
        $this->addSql('DROP TABLE place');
    }
}
