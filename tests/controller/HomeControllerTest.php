<?php
namespace Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function homeUrls()
    {
        return array(
            array('/'),
            array('/Parrain'),
            array('/Contact'),
            array('/Partenaire'),
            array('/Statut'),
            array('/Actualites'),
        );
    }

    /**
     * @dataProvider homeUrls
     */
    public function testHomeControllerUrl($url)
    {
        $this->client->request('GET', $url);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }


}