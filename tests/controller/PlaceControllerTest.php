<?php
/**
 * Created by PhpStorm.
 * User: ayman
 * Date: 12/01/2019
 * Time: 13:58
 */

namespace App\Tests\controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PlaceControllerTest extends WebTestCase
{

    private $client = null;


    public function setUp()
    {
        $this->client = static::createClient();
    }


    public function testAddPlace(){
        $crawler =  $this->client->request('GET', '/admin/AjouterLieu');

        $form = $crawler->selectButton('Ajouter')->form();

        $form['place[namePlace]'] = 'dsdfdsf';
        $form['place[address]'] = 'sdfdsfdsfds';
        $form['place[town]'] = 'lille';
        $form['place[postalCode]'] = '75018';
        $form['place[phone]'] = '145124578';
        $form['place[price]'] = '78';

       $this->client->submit($form);
        $this->client->followRedirect();
    }

}