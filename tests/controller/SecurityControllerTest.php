<?php
/**
 * Created by PhpStorm.
 * User: ayman
 * Date: 11/01/2019
 * Time: 21:13
 */

namespace App\Tests\controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    private $client = null;


    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testSecurityControllerUrl()
    {
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }


    /**
     * test if admin can login and see if page admin is up
     */
    public function testLoginForm(){
        $crawler = $this->client->request('GET', '/login');

        $form = $crawler->selectButton('login')->form();

        $form['_username'] = 'admin@admin.fr';
        $form['_password'] = 'admin';


        $crawler = $this->client->submit($form);
        $crawler = $this->client->followRedirect();

       $this->client->request('GET', '/admin');
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }


}